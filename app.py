from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os

load_dotenv()

app = Flask(__name__)

uri = os.getenv('URI')
user = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=(user, password),database="neo4j")

def get_employees(tx):
    query = "MATCH (m:Employee) RETURN m"
    results = tx.run(query).data()
    employees = [{'name': result['m']['name'], 'birthyear': result['m']['birthyear'], 'position': result['m']['position']} for result in results]
    return employees

@app.route('/employees', methods=['GET'])
def get_employees_route():
    with driver.session() as session:
        employees = session.read_transaction(get_employees)

    response = {'employees': employees}
    return jsonify(response)

def get_Employee(tx, name):
    query = "MATCH (m:Employee) WHERE m.name=$name RETURN m"
    result = tx.run(query, name=name).data()

    if not result:
        return None
    else:
        return {'name': result[0]['m']['name'], 'birthyear': result[0]['m']['birthyear'], 'position': result['m']['position']}

@app.route('/employees/<string:name>', methods=['GET'])
def get_Employee_route(name):
    with driver.session() as session:
        Employee = session.read_transaction(get_Employee, name)

    if not Employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'Employee': Employee}
        return jsonify(response)

def add_Employee(tx, name, year, position):
    query = "CREATE (m:Employee {name: $name, birthyear: $birthyear, position: position})"
    tx.run(query, name=name, birthyear=year, position=position)


@app.route('/employees', methods=['POST'])
def add_Employee_route():
    name = request.json['name']
    year = request.json['birthyear']
    position = request.json['position']

    with driver.session() as session:
        session.write_transaction(add_Employee, name, year, position)

    response = {'status': 'success'}
    return jsonify(response)


def update_Employee(tx, name, new_name, new_year, new_position):
    query = "MATCH (m:Employee) WHERE m.name=$name RETURN m"
    result = tx.run(query, name=name).data()

    if not result:
        return None
    else:
        query = "MATCH (m:Employee) WHERE m.name=$name SET m.name=$new_name, m.birthyear=$new_year, m.position=$new_position"
        tx.run(query, name=name, new_name=new_name, new_year=new_year, new_position=new_position)
        return {'name': new_name, 'year': new_year, 'position': new_position}


@app.route('/employees/<string:name>', methods=['PUT'])
def update_Employee_route(name):
    new_name = request.json['name']
    new_year = request.json['birthyear']
    new_position = request.json['position']

    with driver.session() as session:
        Employee = session.write_transaction(update_Employee, name, new_name, new_year, new_position)

    if not Employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def delete_Employee(tx, name):
    query = "MATCH (m:Employee) WHERE m.name=$name RETURN m"
    result = tx.run(query, name=name).data()

    if not result:
        return None
    else:
        query = "MATCH (m:Employee) WHERE m.name=$name DETACH DELETE m"
        tx.run(query, name=name)
        return {'name': name}

@app.route('/employees/<string:name>', methods=['DELETE'])
def delete_Employee_route(name):
    with driver.session() as session:
        Employee = session.write_transaction(delete_Employee, name)

    if not Employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def get_Employee_subordinates(tx, name):
    query = "MATCH (m:Employee)-[:MANAGES]->(n:Employee) WHERE m.name=$name RETURN n"
    results = tx.run(query, name=name).data()
    subordinates = [{'name': result['n']['name'], 'birthyear': result['n']['birthyear'], 'position': result['n']['position']} for result in results]
    return subordinates

@app.route('/employees/<string:name>/subordinates', methods=['GET'])
def get_Employee_subordinates_route(name):
    with driver.session() as session:
        subordinates = session.read_transaction(get_Employee_subordinates, name)

    response = {'subordinates': subordinates}
    return jsonify(response)


def get_departments(tx):
    query = "MATCH (m:Department) RETURN m"
    results = tx.run(query).data()
    departments = [{'name': result['m']['name'], 'loaction': result['m']['location']} for result in results]
    return departments

@app.route('/departments', methods=['GET'])
def get_departments_route():
    with driver.session() as session:
        departments = session.read_transaction(get_departments)

    response = {'departments': departments}
    return jsonify(response)


def get_Department_Employees(tx, name):
    query = "MATCH (m:Department)<-[:WORKS_IN]-(n:Employee) WHERE m.name=$name RETURN n"
    results = tx.run(query, name=name).data()
    Employees = [{'name': result['n']['name'], 'birthyear': result['n']['birthyear'], 'position': result['n']['position']} for result in results]
    return Employees

@app.route('/departments/<string:name>/Employees', methods=['GET'])
def get_Department_Employees_route(name):
    with driver.session() as session:
        Employees = session.read_transaction(get_Department_Employees, name)

    response = {'Employees': Employees}
    return jsonify(response)

if __name__ == '__main__':
    app.run()

